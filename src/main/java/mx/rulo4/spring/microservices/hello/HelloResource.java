package mx.rulo4.spring.microservices.hello;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

// Indicate this is a controller
@RestController
public class HelloResource {

  @Autowired private MessageSource messageSource;

  // Indicate the resource path and method
  // Alternatively @GetMapping(path = "/say-hello")
  @GetMapping(path = "/say-hello")
  public String sayHello() {
    return "Hello!";
  }

  // Returning a bean instead of String
  @GetMapping(path = "/say-hello-bean")
  public HelloBean sayHelloBean() {
    return HelloBean.builder().message("Hello!").build();
  }

  // Using path variables
  @GetMapping(path = "/say-hello-bean/{name}")
  public HelloBean sayHelloBean(@PathVariable String name) {
    return HelloBean.builder().message(String.format("Hello, %s!", name)).build();
  }

  @Operation(description = "Returns a greeting in the selected language")
  @GetMapping(
      path = "/say-hello-i18n",
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  public HelloBean sayHelloInMyLang() {
    return HelloBean.builder()
        .message(
            messageSource.getMessage("greetings.message", null, LocaleContextHolder.getLocale()))
        .build();
  }
}
