package mx.rulo4.spring.microservices.hello;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
@ToString
@Builder
public class HelloBean {
  //Must have a getter in order to can be returned
  private final String message;
  
  private final String shown;
  
  //Not returned
  @Getter(AccessLevel.NONE)
  private final String hidden;
}
