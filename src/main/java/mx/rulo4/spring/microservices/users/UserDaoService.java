package mx.rulo4.spring.microservices.users;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserDaoService {

  private static List<User> users = new ArrayList<>();
  private static Long usersCount;

  static {
    users.add(
        User.builder()
            .id(1L)
            .name("Adam")
            .birthDate(LocalDate.parse("1990-01-01"))
            .creation(Instant.now())
            .build());
    users.add(
        User.builder()
            .id(2L)
            .name("Eve")
            .birthDate(LocalDate.parse("1990-02-01"))
            .creation(Instant.now())
            .build());
    users.add(
        User.builder()
            .id(3L)
            .name("Jack")
            .birthDate(LocalDate.parse("1990-03-01"))
            .creation(Instant.now())
            .build());

    usersCount = 3L;
  }

  public List<User> findAll() {
    return users;
  }

  public Optional<User> find(Long id) {
    return users.stream().filter(user -> user.getId() == id).findFirst();
  }

  public User save(User user) {
    user.setId(++usersCount);
    user.setCreation(Instant.now());
    users.add(user);
    return user;
  }

  public Optional<User> delete(Long id) {
    Optional<User> userToDelete = find(id);

    if (userToDelete.isPresent()) {
      users.removeIf(user -> user.getId() == id);
    }
    return userToDelete;
  }
}
