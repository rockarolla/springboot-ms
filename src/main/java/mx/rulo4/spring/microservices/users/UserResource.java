package mx.rulo4.spring.microservices.users;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import mx.rulo4.spring.microservices.users.posts.Post;
import mx.rulo4.spring.microservices.users.posts.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class UserResource {

  @Autowired private UserDaoService service;
  @Autowired private UserRepository userRepository;
  @Autowired private PostRepository postRepository;

  private static final String USER_NOT_FOUND_MESSAGE = "No user found with id: %s";

  @GetMapping("/static/users")
  public ResponseEntity<MappingJacksonValue> getAllFromDao() {

    List<User> users = this.service.findAll();

    SimpleBeanPropertyFilter propertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept("name");
    FilterProvider filterProvider =
        new SimpleFilterProvider().addFilter("allUsersFilter", propertyFilter);

    MappingJacksonValue usersMapping = new MappingJacksonValue(users);
    usersMapping.setFilters(filterProvider);

    return ResponseEntity.ok(usersMapping);
  }

  @GetMapping("/static/users/{id}")
  public ResponseEntity<EntityModel<User>> getFromDao(@PathVariable Long id) {
    Optional<User> foundUser = this.service.find(id);

    if (!foundUser.isPresent()) {
      throw new UserNotFoundException(String.format(USER_NOT_FOUND_MESSAGE, id));
    } else {
      EntityModel<User> user = new EntityModel<>(foundUser.get());

      user.add(
          WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).get(id))
              .withSelfRel());
      user.add(
          WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAll())
              .withRel("users"));

      return ResponseEntity.ok(user);
    }
  }

  @PostMapping("/static/users")
  public ResponseEntity<User> createToDao(@Valid @RequestBody User user) {
    User createdUser = service.save(user);
    URI createdLocation =
        ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(createdUser.getId())
            .toUri();

    return ResponseEntity.created(createdLocation).body(createdUser);
  }

  @DeleteMapping("/static/users/{id}")
  public void deleteFromDao(@PathVariable Long id) {
    Optional<User> deletedUser = service.delete(id);
    if (!deletedUser.isPresent()) {
      throw new UserNotFoundException(String.format(USER_NOT_FOUND_MESSAGE, id));
    }
  }

  @GetMapping("/users")
  public ResponseEntity<List<User>> getAll() {
    List<User> users = this.userRepository.findAll();
    return ResponseEntity.ok(users);
  }

  @GetMapping("/users/{id}")
  public ResponseEntity<EntityModel<User>> get(@PathVariable Long id) {
    Optional<User> foundUser = this.userRepository.findById(id);

    if (!foundUser.isPresent()) {
      throw new UserNotFoundException(String.format(USER_NOT_FOUND_MESSAGE, id));
    } else {
      EntityModel<User> user = new EntityModel<>(foundUser.get());

      user.add(
          WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).get(id))
              .withSelfRel());
      user.add(
          WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAll())
              .withRel("users"));

      return ResponseEntity.ok(user);
    }
  }

  @PostMapping("/users")
  public ResponseEntity<User> create(@Valid @RequestBody User user) {
    User createdUser = userRepository.save(user);
    URI createdLocation =
        ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(createdUser.getId())
            .toUri();

    return ResponseEntity.created(createdLocation).body(createdUser);
  }

  @DeleteMapping("/users/{id}")
  public void delete(@PathVariable Long id) {
    userRepository.deleteById(id);
  }

  @GetMapping("/users/{id}/posts")
  public List<Post> getUserPosts(@PathVariable Long id) {
    Optional<User> user = userRepository.findById(id);

    if (!user.isPresent()) {
      throw new UserNotFoundException(String.format(USER_NOT_FOUND_MESSAGE, id));
    } else {
      return user.get().getPosts();
    }
  }

  @PostMapping("/users/{id}/posts")
  public ResponseEntity<Post> createUserPost(@PathVariable Long id, @Valid @RequestBody Post post) {
    Optional<User> user = userRepository.findById(id);

    if (!user.isPresent()) {
      throw new UserNotFoundException(String.format(USER_NOT_FOUND_MESSAGE, id));
    } else {
      post.setUser(user.get());
      Post createdPost = postRepository.save(post);

      URI createdLocation =
          ServletUriComponentsBuilder.fromCurrentRequest()
              .path("/{id}")
              .buildAndExpand(createdPost.getId())
              .toUri();

      return ResponseEntity.created(createdLocation).body(post);
    }
  }
}
