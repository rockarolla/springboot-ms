package mx.rulo4.spring.microservices.users.posts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mx.rulo4.spring.microservices.users.User;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@NoArgsConstructor
@ToString(exclude = "user")
@Entity
public class Post {

  @Id @GeneratedValue private Long id;

  private String content;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private User user;
}
