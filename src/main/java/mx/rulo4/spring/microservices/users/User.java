package mx.rulo4.spring.microservices.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mx.rulo4.spring.microservices.users.posts.Post;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "User basic data")
@JsonIgnoreProperties(value = {"active"})
// @JsonFilter("allUsersFilter")
@Entity
public class User {

  @Id @GeneratedValue private Long id;

  @Size(min = 3, message = "Name must contain at least 3 characters")
  @Schema(description = "Name must contain at least 3 characters")
  private String name;

  @Past(message = "Birth date must be a past date")
  @Schema(description = "Birth date must be a past date")
  private LocalDate birthDate;

  @Schema(description = "Date when user was registered")
  @JsonIgnore
  private Instant creation;

  @Schema(description = "User account status")
  private boolean active;

  @OneToMany(mappedBy = "user")
  private List<Post> posts;
}
