package mx.rulo4.spring.microservices.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;

@Getter
@AllArgsConstructor
public class ExceptionResponse {
  
  private Instant timestamp;
  private String message;
  private String detail;
}
