package mx.rulo4.spring.microservices.common;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiDocumentationConfig {

  @Bean
  public OpenAPI customOpenAPI() {
    return new OpenAPI()
        .components(new Components())
        .info(
            new Info()
                .version("0.1")
                .title("Spring micro-services tests")
                .description("Playing with springboot to create micro-services")
                .contact(new Contact().name("Raúl " + "García").email("raul4@live.com.mx")));
  }
}
