insert into user (id, name, birth_date, creation, active) values (10001, 'User1', '2020-01-01', current_timestamp,
true);
insert into user (id, name, birth_date, creation, active) values (10002, 'User2', '2020-02-02', current_timestamp, true);
insert into user (id, name, birth_date, creation, active) values (10003, 'User3', '2020-03-03', current_timestamp, true);

insert into post (id, content, user_id) values (10001, 'user 1 post 1', 10001);
insert into post (id, content, user_id) values (10002, 'user 1 post 2', 10001);
insert into post (id, content, user_id) values (10003, 'user 1 post 3', 10001);
insert into post (id, content, user_id) values (10004, 'user 2 post 1', 10002);
insert into post (id, content, user_id) values (10005, 'user 2 post 2', 10002);
